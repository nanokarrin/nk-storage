import React from 'react';

export default function Header({title}) {
  return (
    <header>
      <h1>{title.replace('/', '')}</h1>
    </header>
  );
}
