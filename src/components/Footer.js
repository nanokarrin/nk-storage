import React from 'react';
import config from '../../config';
import '../fontawesome';

export default function Footer() {
  return (
    <footer>
      <ul className="icons">
        {config.socialLinks.map(social => {
          const { icon, name, url } = social;
          return (
            <li key={url}>
              <a href={url} >
                <span><i className={`${icon} fa-2x`}/></span>
              </a>
            </li>
          );
        })}
      </ul>
    </footer>
  );
}
